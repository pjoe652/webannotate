import React, { Component } from 'react';
import Oscilloscope from './oscilloscope.jpg'
import './oscilloscope.css';

class oscilloscope extends Component {

  constructor(props) {
    super(props)
    this.state = {
      IOLock : false,

      displayPanel : 0,
      chip : 0,
      headers: 0,
      switches: 0,
      leds: 0,
      powerSwitch : 0,
      powerSupply : 0,
      usbBlaster: 0,
      vgaDac : 0,
      ps2Port : 0,
      SDCard : 0,
      SDRam : 0,
      flash: 0,
      usbBlasterCircuit: 0,
      pushButton: 0,

      displayPanelBool : false,
      chipBool : false,
      headersBool: false,
      switchesBool: false,
      ledsBool: false,
      powerSwitchBool : false,
      powerSupplyBool : false,
      usbBlasterBool : false,
      vgaDacBool : false,
      ps2PortBool : false,
      SDCardBool : false,
      SDRamBool: false,
      flashBool: false,
      usbBlasterCircuitBool: false,
      pushButtonBool: false,

    }
  }

  // ENTERS
  onItemEnter = (item) => {

    // IO Devices
    if (item === "displayPanel") {
      this.setState({
                    displayPanel: 0.2
      })
    } else if(item === "chip") {
      this.setState({
                    chip : 0.2
      })
    } else if(item === "headers") {
      this.setState({
                    headers: 0.2
      })
    } else if(item === "switches") {
      this.setState({
                    switches: 0.2
      })
    } else if(item === "leds") {
      this.setState({
                    leds: 0.2
      })
    } else if(item === "powerSwitch") {
      this.setState({
                    powerSwitch : 0.2
      })
    } else if(item === "powerSupply") {
      this.setState({
                    powerSupply : 0.2
      })
    } else if(item === "usbBlaster") {
      this.setState({
                    usbBlaster : 0.2
      })
    } else if(item === "vgaDac") {
      this.setState({
                    vgaDac : 0.2
      })
    } else if(item === "ps2Port") {
      this.setState({
                    ps2Port : 0.2
      })
    } else if(item === "SDCard") {
      this.setState({
                    SDCard : 0.2
      })
    } else if(item === "SDRam") {
      this.setState({
                    SDRam : 0.2
      })
    } else if(item === "flash") {
      this.setState({
                    flash : 0.2
      })
    } else if(item === "usbBlasterCircuit") {
      this.setState({
                    usbBlasterCircuit : 0.2
      })
    } else if(item === "pushButton") {
      this.setState({
                    pushButton : 0.2
      })
    }
  }

  // LEAVES

  onItemLeave = (item) => {
    if (item === "displayPanel") {
      this.setState({
                    displayPanel: 0
      })
    } else if(item === "chip") {
      this.setState({
                    chip : 0,
      })
    } else if(item === "headers") {
      this.setState({
                    headers: 0,
      })
    } else if(item === "switches") {
      this.setState({
                    switches: 0,
      })
    } else if(item === "leds") {
      this.setState({
                    leds: 0,
      })
    } else if(item === "powerSwitch") {
      this.setState({
                    powerSwitch : 0,
      })
    } else if(item === "powerSupply") {
      this.setState({
                    powerSupply : 0,
      })
    } else if(item === "usbBlaster") {
      this.setState({
                    usbBlaster : 0,
      })
    } else if(item === "vgaDac") {
      this.setState({
                    vgaDac : 0,
      })
    } else if(item === "ps2Port") {
      this.setState({
                    ps2Port : 0,
      })
    } else if(item === "SDCard") {
      this.setState({
                    SDCard : 0,
      })
    } else if(item === "SDRam") {
      this.setState({
                    SDRam : 0,
      })
    } else if(item === "flash") {
      this.setState({
                    flash : 0,
      })
    } else if(item === "usbBlasterCircuit") {
      this.setState({
                    usbBlasterCircuit : 0
      })
    } else if(item === "pushButton") {
      this.setState({
                    pushButton : 0
      })
    }  }

  // CLICK

  onItemClick = (item) => {
    if (item === "displayPanel") {
      this.setState({
                    displayPanelBool : true,
                    chipBool : false,
                    headersBool: false,
                    switchesBool: false,
                    ledsBool: false,
                    powerSwitchBool : false,
                    powerSupplyBool : false,
                    usbBlasterBool : false,
                    vgaDacBool : false,
                    ps2PortBool : false,
                    SDCardBool : false,
                    SDRamBool : false,
                    flashBool: false,
                    usbBlasterCircuitBool: false,
                    pushButtonBool: false,
      })
    } else if(item === "chip") {
      this.setState({
                    displayPanelBool : false,
                    chipBool : true,
                    headersBool: false,
                    switchesBool: false,
                    ledsBool: false,
                    powerSwitchBool : false,
                    powerSupplyBool : false,
                    usbBlasterBool : false,
                    vgaDacBool : false,
                    ps2PortBool : false,
                    SDCardBool : false,
                    SDRamBool : false,
                    flashBool: false,
                    usbBlasterCircuitBool: false,
                    pushButtonBool: false,
      })
    } else if(item === "headers") {
      this.setState({
                    displayPanelBool : false,
                    chipBool : false,
                    headersBool: true,
                    switchesBool: false,
                    ledsBool: false,
                    powerSwitchBool : false,
                    powerSupplyBool : false,
                    usbBlasterBool : false,
                    vgaDacBool : false,
                    ps2PortBool : false,
                    SDCardBool : false,
                    SDRamBool : false,
                    flashBool: false,
                    usbBlasterCircuitBool: false,
                    pushButtonBool: false,
      })
    } else if(item === "switches") {
      this.setState({
                    displayPanelBool : false,
                    chipBool : false,
                    headersBool: false,
                    switchesBool: true,
                    ledsBool: false,
                    powerSwitchBool : false,
                    powerSupplyBool : false,
                    usbBlasterBool : false,
                    vgaDacBool : false,
                    ps2PortBool : false,
                    SDCardBool : false,
                    SDRamBool : false,
                    flashBool: false,
                    usbBlasterCircuitBool: false,
                    pushButtonBool: false,
      })
    } else if(item === "leds") {
      this.setState({
                    displayPanelBool : false,
                    chipBool : false,
                    headersBool: false,
                    switchesBool: false,
                    ledsBool: true,
                    powerSwitchBool : false,
                    powerSupplyBool : false,
                    usbBlasterBool : false,
                    vgaDacBool : false,
                    ps2PortBool : false,
                    SDCardBool : false,
                    SDRamBool : false,
                    flashBool: false,
                    usbBlasterCircuitBool: false,
                    pushButtonBool: false,
      })
    } else if(item === "powerSwitch") {
      this.setState({
                    displayPanelBool : false,
                    chipBool : false,
                    headersBool: false,
                    switchesBool: false,
                    ledsBool: false,
                    powerSwitchBool : true,
                    powerSupplyBool : false,
                    usbBlasterBool : false,
                    vgaDacBool : false,
                    ps2PortBool : false,
                    SDCardBool : false,
                    SDRamBool : false,
                    flashBool: false,
                    usbBlasterCircuitBool: false,
                    pushButtonBool: false,
      })
    } else if(item === "powerSupply") {
      this.setState({
                    displayPanelBool : false,
                    chipBool : false,
                    headersBool: false,
                    switchesBool: false,
                    ledsBool: false,
                    powerSwitchBool : false,
                    powerSupplyBool : true,
                    usbBlasterBool : false,
                    vgaDacBool : false,
                    ps2PortBool : false,
                    SDCardBool : false,
                    SDRamBool : false,
                    flashBool: false,
                    usbBlasterCircuitBool: false,
                    pushButtonBool: false,
      })
    } else if(item === "usbBlaster") {
      this.setState({
                    displayPanelBool : false,
                    chipBool : false,
                    headersBool: false,
                    switchesBool: false,
                    ledsBool: false,
                    powerSwitchBool : false,
                    powerSupplyBool : false,
                    usbBlasterBool : true,
                    vgaDacBool : false,
                    ps2PortBool : false,
                    SDCardBool : false,
                    SDRamBool : false,
                    flashBool: false,
                    usbBlasterCircuitBool: false,
                    pushButtonBool: false,
      })
    } else if(item === "vgaDac") {
      this.setState({
                    displayPanelBool : false,
                    chipBool : false,
                    headersBool: false,
                    switchesBool: false,
                    ledsBool: false,
                    powerSwitchBool : false,
                    powerSupplyBool : false,
                    usbBlasterBool : false,
                    vgaDacBool : true,
                    ps2PortBool : false,
                    SDCardBool : false,
                    SDRamBool : false,
                    flashBool: false,
                    usbBlasterCircuitBool: false,
                    pushButtonBool: false,
      })
    } else if(item === "ps2Port") {
      this.setState({
                    displayPanelBool : false,
                    chipBool : false,
                    headersBool: false,
                    switchesBool: false,
                    ledsBool: false,
                    powerSwitchBool : false,
                    powerSupplyBool : false,
                    usbBlasterBool : false,
                    vgaDacBool : false,
                    ps2PortBool : true,
                    SDCardBool : false,
                    SDRamBool : false,
                    flashBool: false,
                    usbBlasterCircuitBool: false,
                    pushButtonBool: false,
      })
    } else if(item === "SDCard") {
      this.setState({
                    displayPanelBool : false,
                    chipBool : false,
                    headersBool: false,
                    switchesBool: false,
                    ledsBool: false,
                    powerSwitchBool : false,
                    powerSupplyBool : false,
                    usbBlasterBool : false,
                    vgaDacBool : false,
                    ps2PortBool : false,
                    SDCardBool : true,
                    SDRamBool : false,
                    flashBool: false,
                    usbBlasterCircuitBool: false,
                    pushButtonBool: false,
      })
    } else if(item === "SDRam") {
      this.setState({
                    displayPanelBool : false,
                    chipBool : false,
                    headersBool: false,
                    switchesBool: false,
                    ledsBool: false,
                    powerSwitchBool : false,
                    powerSupplyBool : false,
                    usbBlasterBool : false,
                    vgaDacBool : false,
                    ps2PortBool : false,
                    SDCardBool : false,
                    SDRamBool : true,
                    flashBool: false,
                    usbBlasterCircuitBool: false,
                    pushButtonBool: false,
      })
    } else if(item === "flash") {
      this.setState({
                    displayPanelBool : false,
                    chipBool : false,
                    headersBool: false,
                    switchesBool: false,
                    ledsBool: false,
                    powerSwitchBool : false,
                    powerSupplyBool : false,
                    usbBlasterBool : false,
                    vgaDacBool : false,
                    ps2PortBool : false,
                    SDCardBool : false,
                    SDRamBool : false,
                    flashBool: true,
                    usbBlasterCircuitBool: false,
                    pushButtonBool: false,
      })
    } else if(item === "usbBlasterCircuit") {
      this.setState({
                    displayPanelBool : false,
                    chipBool : false,
                    headersBool: false,
                    switchesBool: false,
                    ledsBool: false,
                    powerSwitchBool : false,
                    powerSupplyBool : false,
                    usbBlasterBool : false,
                    vgaDacBool : false,
                    ps2PortBool : false,
                    SDCardBool : false,
                    SDRamBool : false,
                    flashBool: false,
                    usbBlasterCircuitBool: true,
                    pushButtonBool: false,
      })
    } else if(item === "pushButton") {
      this.setState({
                    displayPanelBool : false,
                    chipBool : false,
                    headersBool: false,
                    switchesBool: false,
                    ledsBool: false,
                    powerSwitchBool : false,
                    powerSupplyBool : false,
                    usbBlasterBool : false,
                    vgaDacBool : false,
                    ps2PortBool : false,
                    SDCardBool : false,
                    SDRamBool : false,
                    flashBool: false,
                    usbBlasterCircuitBool: false,
                    pushButtonBool: true,
      })
    }
  }

  //Button Select

  IOButtonClick = () => {
    this.setState({
      displayPanel : 0.2,
      chip : 0,
      headers: 0.2,
      switches: 0.2,
      leds: 0.2,
      powerSwitch : 0.2,
      powerSupply : 0.2,
      usbBlaster: 0.2,
      vgaDac : 0.2,
      ps2Port : 0.2,
      SDCard : 0.2,
      SDRam : 0,
      flash: 0,
      usbBlasterCircuit: 0,
      pushButton: 0.2,
    })
  }


  render() {
    return (
      <div className="App">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossOrigin="anonymous"/>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossOrigin="anonymous"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossOrigin="anonymous"/>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossOrigin="anonymous"/>
        <header className="App-header">
        <div className="display-holder">
        <div className="Board-holder">
          <img src={Oscilloscope} className="Board"/>


            {/*7-Seg Display*/}
            <div className="display-panel" style={{opacity: this.state.displayPanel}} 
            onMouseEnter={() => this.onItemEnter("displayPanel")} onMouseLeave={() => this.onItemLeave("displayPanel")} onClick={() => this.onItemClick("displayPanel")}>
            </div>


            {/*FPGA*/}
            <div className="chip" style={{opacity: this.state.chip}} 
            onMouseEnter={() => this.onItemEnter("chip")} onMouseLeave={() => this.onItemLeave("chip")} onClick={() => this.onItemClick("chip")}>
            </div>

            {/*Expansion Headers*/}
            <div className="headers" style={{opacity: this.state.headers}} 
            onMouseEnter={() => this.onItemEnter("headers")} onMouseLeave={() => this.onItemLeave("headers")} onClick={() => this.onItemClick("headers")}>
            </div>

            {/*Switches*/}
            <div className="switches" style={{opacity: this.state.switches}} 
            onMouseEnter={() => this.onItemEnter("switches")} onMouseLeave={() => this.onItemLeave("switches")} onClick={() => this.onItemClick("switches")}>
            </div>

            {/*LEDs*/}
            <div className="leds" style={{opacity: this.state.leds}} 
            onMouseEnter={() => this.onItemEnter("leds")} onMouseLeave={() => this.onItemLeave("leds")} onClick={() => this.onItemClick("leds")}>
            </div>

            {/*Power Switch*/}
            <div className="power-switch" style={{opacity: this.state.powerSwitch}} 
            onMouseEnter={() => this.onItemEnter("powerSwitch")} onMouseLeave ={() => this.onItemLeave("powerSwitch")} onClick={() => this.onItemClick("powerSwitch")}>
            </div>

            {/*Power Supply*/}
            <div className="power-supply" style={{opacity: this.state.powerSupply}} 
            onMouseEnter={() => this.onItemEnter("powerSupply")} onMouseLeave ={() => this.onItemLeave("powerSupply")} onClick={() => this.onItemClick("powerSupply")}>
            </div>

            {/*USB Blaster*/}
            <div className="usb-blaster" style={{opacity: this.state.usbBlaster}} 
            onMouseEnter={() => this.onItemEnter("usbBlaster")} onMouseLeave ={() => this.onItemLeave("usbBlaster")} onClick={() => this.onItemClick("usbBlaster")}>
            </div>

            {/*VGA DAC*/}
            <div className="vga-dac" style={{opacity: this.state.vgaDac}} 
            onMouseEnter={() => this.onItemEnter("vgaDac")} onMouseLeave ={() => this.onItemLeave("vgaDac")} onClick={() => this.onItemClick("vgaDac")}>
            </div>

            {/*PS/2 Port*/}
            <div className="ps2-port" style={{opacity: this.state.ps2Port}} 
            onMouseEnter={() => this.onItemEnter("ps2Port")} onMouseLeave ={() => this.onItemLeave("ps2Port")} onClick={() => this.onItemClick("ps2Port")}>
            </div>

            {/*SD Card*/}
            <div className="sd-card" style={{opacity: this.state.SDCard}} 
            onMouseEnter={() => this.onItemEnter("SDCard")} onMouseLeave ={() => this.onItemLeave("SDCard")} onClick={() => this.onItemClick("SDCard")}>
            </div>

            {/*SD Ram*/}
            <div className="sdram" style={{opacity: this.state.SDRam}} 
            onMouseEnter={() => this.onItemEnter("SDRam")} onMouseLeave ={() => this.onItemLeave("SDRam")} onClick={() => this.onItemClick("SDRam")}>
            </div>

            {/*Flash*/}
            <div className="flash" style={{opacity: this.state.flash}} 
            onMouseEnter={() => this.onItemEnter("flash")} onMouseLeave ={() => this.onItemLeave("flash")} onClick={() => this.onItemClick("flash")}>
            </div>

            {/*USB Blaster Circuit*/}
            <div className="usb-blaster-circuit" style={{opacity: this.state.usbBlasterCircuit}} 
            onMouseEnter={() => this.onItemEnter("usbBlasterCircuit")} onMouseLeave ={() => this.onItemLeave("usbBlasterCircuit")} onClick={() => this.onItemClick("usbBlasterCircuit")}>
            </div>

            {/*Push Button*/}
            <div className="push-button" style={{opacity: this.state.pushButton}} 
            onMouseEnter={() => this.onItemEnter("pushButton")} onMouseLeave ={() => this.onItemLeave("pushButton")} onClick={() => this.onItemClick("pushButton")}>
            </div>
            </div>

            {
              // 7-Segment Display
              this.state.displayPanelBool === true ?
              <div className="info-box">
                <h5 style={{color: 'black'}}>7-Segment Display</h5>
                <h6 style={{color: 'black'}}>I/O</h6>
                <p style={{color: 'black'}}>The DE0 board has four 7-segment displays. These displays are arranged into two pairs and a group
of four, with the intent of displaying numbers of various sizes. Applying a low logic level to a segment
causes it to light up, and applying a high logic level turns it off. </p>
              </div>

              //  Cyclone III
              : this.state.chipBool === true ?
              <div className="info-box">
                <h5 className="card-title" style={{color: 'black'}}>Cyclone III</h5>
                <h6 className="card-subtitle mb-2 text-muted" style={{color: 'black'}}>FPGA</h6>
                <p className="card-text" style={{color: 'black'}}>Cyclone III device family includes a customer-defined feature set that is optimized for
portable applications and offers a wide range of density, memory, embedded
multiplier, and I/O options</p>
              </div>

              // Expansion Headers
              : this.state.headersBool === true ? 
              <div className="info-box">
                <h5 className="card-title" style={{color: 'black'}}>Expansion Headers</h5>
                <h6 className="card-subtitle mb-2 text-muted" style={{color: 'black'}}>I/O</h6>
                <p className="card-text" style={{color: 'black'}}>The DE0 Board provides two 40-pin expansion headers. Each header connects directly to 36 pins of
the Cyclone III FPGA, and also provides DC +5V (VCC5), DC +3.3V (VCC33), and two GND pins.
Among these 36 I/O pins, 4 pins are connected to the PLL clock input and output pins of the FPGA
allowing the expansion daughter cards to access the PLL blocks in the FPGA. </p>
              </div>

              // Switches
              : this.state.switchesBool === true ?
              <div className="info-box">
                <h5 className="card-title" style={{color: 'black'}}>Switches</h5>
                <h6 className="card-subtitle mb-2 text-muted" style={{color: 'black'}}>I/O</h6>
                <p className="card-text" style={{color: 'black'}}>The DE0 Board provides 10 slide switches. 
                A switch causes logic 0 when in the DOWN position and logic 1 when in the UP position </p>
              </div>

              // LEDs
              : this.state.ledsBool === true ?
              <div className="info-box">
                <h5 className="card-title" style={{color: 'black'}}>LEDs</h5>
                <h6 className="card-subtitle mb-2 text-muted" style={{color: 'black'}}>I/O</h6>
                <p className="card-text" style={{color: 'black'}}>The DE0 Board provides 10 green color LEDs. Each LED is active high.</p>
              </div>

              // Power Switch
              : this.state.powerSwitchBool === true ?
              <div className="info-box">
                <h5 className="card-title" style={{color: 'black'}}>Power Switch</h5>
                <h6 className="card-subtitle mb-2 text-muted" style={{color: 'black'}}>I/O</h6>
                <p className="card-text" style={{color: 'black'}}>Info.</p>
              </div>

              // Power Supply
              : this.state.powerSupplyBool === true ?
              <div className="info-box">
                <h5 className="card-title" style={{color: 'black'}}>Power Supply</h5>
                <h6 className="card-subtitle mb-2 text-muted" style={{color: 'black'}}>I/O</h6>
                <p className="card-text" style={{color: 'black'}}>The DE0 Board should be connected to a 7.5V power supply.</p>
              </div>

              // USB Blaster
              : this.state.usbBlasterBool === true ?
              <div className="info-box">
                <h5 className="card-title" style={{color: 'black'}}>USB Blaster</h5>
                <h6 className="card-subtitle mb-2 text-muted" style={{color: 'black'}}>I/O</h6>
                <p className="card-text" style={{color: 'black'}}>The DE0 Board provides a built-in USB Blaster Circuit. This allows for on-board USB Blaster 
for programming and user API (Application programming interface) control using the Altera EMP240 CPLD</p>
              </div>

              // VGA Output
              : this.state.vgaDacBool === true ?
              <div className="info-box">
                <h5 className="card-title" style={{color: 'black'}}>VGA Output</h5>
                <h6 className="card-subtitle mb-2 text-muted" style={{color: 'black'}}>I/O</h6>
                <p className="card-text" style={{color: 'black'}}>The DE0 Board provides a VGA output which uses a 4-bit resistor-network DAC
 with 15-pin high-density D-sub connector and supports up to 1280x1024 at 60-Hz refresh rate</p>
              </div>

              // PS/2 Port
              : this.state.ps2PortBool === true ?
              <div className="info-box">
                <h5 className="card-title" style={{color: 'black'}}>PS/2 Port</h5>
                <h6 className="card-subtitle mb-2 text-muted" style={{color: 'black'}}>I/O</h6>
                <p className="card-text" style={{color: 'black'}}>The DE0 Board provides a PS/2 serial port which can be used through a PS/2 Y Cable to 
                allow you to connect a keyboard and mouse to one port</p>
              </div>

              // SD Card
              : this.state.SDCardBool === true ?
              <div className="info-box">
                <h5 className="card-title" style={{color: 'black'}}>SD Card</h5>
                <h6 className="card-subtitle mb-2 text-muted" style={{color: 'black'}}>I/O</h6>
                <p className="card-text" style={{color: 'black'}}>The DE0 Board provides a SD card socket which provides both SPI and SD 1-bit mod SD Card access </p>
              </div>

              // SDRAM
              : this.state.SDRamBool === true ?
              <div className="info-box">
                <h5 className="card-title" style={{color: 'black'}}>SDRAM</h5>
                <h6 className="card-subtitle mb-2 text-muted" style={{color: 'black'}}>Memory</h6>
                <p className="card-text" style={{color: 'black'}}>The DE0 Board provides a SDRAM. The SDRAM is an 8-Mbyte Single Data Rate 
                Synchronous Dynamic RAM memory chip which supports 16-bits data bus </p>
              </div>

              // Flash Memory
              : this.state.flashBool === true ?
              <div className="info-box">
                <h5 className="card-title" style={{color: 'black'}}>Flash Memory</h5>
                <h6 className="card-subtitle mb-2 text-muted" style={{color: 'black'}}>Memory</h6>
                <p className="card-text" style={{color: 'black'}}>The DE0 Board provides flash memory. The flash memory is a 4-Mbyte NOR Flash memory 
                which support Byte (8-bits)/Word (16-bits) mode </p>
              </div>

              // USB Blaster Circuit
              : this.state.usbBlasterCircuitBool === true ?
              <div className="info-box">
                <h5 className="card-title" style={{color: 'black'}}>USB Blaster Circuit</h5>
                <h6 className="card-subtitle mb-2 text-muted" style={{color: 'black'}}>Circuit</h6>
                <p className="card-text" style={{color: 'black'}}>The DE0 Board provides a built-in USB Blaster Circuit. This allows for on-board USB Blaster 
for programming and user API (Application programming interface) control using the Altera EMP240 CPLD</p>
              </div>

              // Push Button
              : this.state.pushButtonBool === true ?
              <div className="info-box">
                <h5 className="card-title" style={{color: 'black'}}>Push Button</h5>
                <h6 className="card-subtitle mb-2 text-muted" style={{color: 'black'}}>I/O</h6>
                <p className="card-text" style={{color: 'black'}}>The DE0 Board provides three pushbutton switches. Each which are normally high;
                generates one active-low pulse when the switch is pressed</p>
              </div>
              :
                <a>
                </a>
            }
          <div className="button-container">
            <button type="button" className="btn btn-primary" style={{width: '33%', margin: '10px'}} onClick={this.IOButtonClick}>I/O</button>
            <button type="button" className="btn btn-primary" style={{width: '33%', margin: '10px'}}>Display</button>
            <button type="button" className="btn btn-primary" style={{width: '33%', margin: '10px'}}>Circuit</button>
          </div>




          </div>

        </header>
      </div>
    );
  }
}

export default oscilloscope;
