import React, { Component } from 'react';
import logo from './logo.svg';
import De0board from './component/de0Board/de0Board';
import Oscilloscope from './component/oscilloscope/oscilloscope';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
       itemSelect: "de0board",
    }
  }

  selectImage = (item) => {
    if (item === "de0board") {
      this.setState({
        itemSelect : "de0board"
      })
    } else if (item === "oscilloscope") {
      this.setState({
        itemSelect : "oscilloscope"
      })
    }
  }

  render() {
    const { itemSelect } = this.state;
    return (
      <div className="App">
        <div className="button-container">
          <button type="button" className="btn btn-primary" style={{width: '50%', margin: '10px'}} onClick={() => this.selectImage("de0board")}>DE0 Board</button>
          <button type="button" className="btn btn-primary" style={{width: '50%', margin: '10px'}} onClick={() => this.selectImage("oscilloscope")}>Oscilloscope</button>
        </div>
        {
          itemSelect === "de0board" ?
            <De0board/>
          :
            <Oscilloscope/>
        }
        
      </div>
    );
  }
}

export default App;
